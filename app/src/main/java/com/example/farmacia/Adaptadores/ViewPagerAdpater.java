package com.example.farmacia.Adaptadores;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.farmacia.Fragments.FarmaciasFragment;
import com.example.farmacia.Fragments.InfoProductoFragment;
import com.example.farmacia.Interfaces.Icomunicafragments;

public class ViewPagerAdpater extends FragmentStateAdapter {

    private String[] titulos=new String[]{"info","farmacia"};
    private Icomunicafragments icomunicafragments;

    public ViewPagerAdpater(@NonNull FragmentActivity fragmentActivity, Icomunicafragments icomunicafragments) {
        super(fragmentActivity);
        this.icomunicafragments=icomunicafragments;

    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new InfoProductoFragment(icomunicafragments);
            case 1:
                return new FarmaciasFragment();
        }

        return new InfoProductoFragment(icomunicafragments);
    }

    @Override
    public int getItemCount() {
        return titulos.length;
    }
}
