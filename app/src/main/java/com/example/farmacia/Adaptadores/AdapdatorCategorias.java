package com.example.farmacia.Adaptadores;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmacia.Entidades.Categorias;
import com.example.farmacia.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class AdapdatorCategorias extends RecyclerView.Adapter<AdapdatorCategorias.ViewHolder> {

    ArrayList<Categorias> categorias;
    ArrayList<Categorias> categoriasoriginal;

    public AdapdatorCategorias(ArrayList<Categorias> categorias) {
        this.categorias=new ArrayList<>();
        this.categoriasoriginal = categorias;
        this.categorias.addAll(categoriasoriginal);

    }


    public void filtrado(String buscar){
        if(buscar.length()==0){
            categorias.clear();
            categorias.addAll(categoriasoriginal);
        }else{
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                List<Categorias> coleccion=categorias.stream().
                       filter(i->i.getC_name().toLowerCase().contains(buscar.toLowerCase())).
                       collect(Collectors.toList());
                categorias.clear();
                categorias.addAll(coleccion);
            }else{
                categorias.clear();
                for (Categorias c:categorias){
                    if(c.getC_name().toLowerCase().contains(buscar.toLowerCase())){
                        categorias.add(c);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapdatorCategorias.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemcategorias,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapdatorCategorias.ViewHolder holder, int position) {
        holder.txtnombre.setText(categorias.get(position).getC_name());
        holder.txtdetalle.setText(categorias.get(position).getC_det());

    }

    @Override
    public int getItemCount() {
        return categorias.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtnombre,txtdetalle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtnombre=itemView.findViewById(R.id.txtcategoriasnombre);
            txtdetalle=itemView.findViewById(R.id.txtcategoriadetalle);
        }
    }
}
