package com.example.farmacia.Adaptadores;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.farmacia.Entidades.Categorias;
import com.example.farmacia.Entidades.Productos;
import com.example.farmacia.R;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AdaptadorProductos extends RecyclerView.Adapter<AdaptadorProductos.ViewHolder> {
    ArrayList<Productos> productos;
    ArrayList<Productos> productosoriginal;

    public AdaptadorProductos(ArrayList<Productos> productos) {
        this.productos = new ArrayList<>();
        this.productosoriginal=productos;
        this.productos.addAll(productosoriginal);
    }

    public void filtrado(String buscar){
        if(buscar.length()==0){
            productos.clear();
            productos.addAll(productosoriginal);
        }else{
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                List<Productos> coleccion=productos.stream().
                        filter(i->i.getP_name().toLowerCase().contains(buscar.toLowerCase())).
                        collect(Collectors.toList());
                productos.clear();
                productos.addAll(coleccion);
            }else{
                productos.clear();
                for (Productos c:productos){
                    if(c.getP_name().toLowerCase().contains(buscar.toLowerCase())){
                        productos.add(c);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdaptadorProductos.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemproductos,null,false);
        return new AdaptadorProductos.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorProductos.ViewHolder holder, int position) {
        holder.txtnombre.setText(productos.get(position).getP_name());
        holder.txtcategoria.setText(productos.get(position).getC_name());
        holder.txtfarmacia.setText(productos.get(position).getF_name());
        holder.txtprecio.setText(productos.get(position).getPr_precio()+"");
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView txtnombre,txtfarmacia,txtcategoria,txtprecio;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtnombre=itemView.findViewById(R.id.txtNombreProductPreview);
            txtcategoria=itemView.findViewById(R.id.txtCategoriaProductPreview);
            txtfarmacia=itemView.findViewById(R.id.txtFarmaciaProductPreview);
            txtprecio=itemView.findViewById(R.id.txtPrecioProductPreview);
        }
    }
}
