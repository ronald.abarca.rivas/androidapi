package com.example.farmacia.Interfaces;

import com.example.farmacia.Entidades.Categorias;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface Icategorias {
     /*@GET("jsonprueba/")
   public Call<List<Xd>> listRepos();*/

    @GET("public/api/t_category")
    public Call<List<Categorias>> listCategorias();
    /*@POST("public/api/t_dep")
    public Call<Xd> guardar(@Body Xd user);
    @PUT("public/api/t_dep")
    public Call<Integer> actualizar(@Body Xd user);
    @DELETE("public/api/t_dep/{id}")
    public Call<Integer> eliminar(@Path("id") int id);*/
}
