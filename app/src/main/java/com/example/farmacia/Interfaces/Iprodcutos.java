package com.example.farmacia.Interfaces;

import com.example.farmacia.Entidades.Categorias;
import com.example.farmacia.Entidades.Productos;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Iprodcutos {
    @GET("public/api/t_productRep")
    public Call<List<Productos>> listProductos();
}
