package com.example.farmacia.Entidades;

public class Categorias {
    private int c_id;
    private String c_name;
    private String c_det;
    private String c_time;

    public Categorias(int c_id, String c_name, String c_det, String c_time) {
        this.c_id = c_id;
        this.c_name = c_name;
        this.c_det = c_det;
        this.c_time = c_time;
    }

    public int getC_id() {
        return c_id;
    }

    public String getC_name() {
        return c_name;
    }

    public String getC_det() {
        return c_det;
    }

    public String getC_time() {
        return c_time;
    }
}
