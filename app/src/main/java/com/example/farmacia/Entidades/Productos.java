package com.example.farmacia.Entidades;

public class Productos {
    private int pr_id;
    private int pr_farmid;
    private int pr_prodid;
    private int pr_cantdisp;
    private float pr_precio;
    private int pr_preciodesc;
    private int pr_available;
    private String pr_timeCreation;
    private int p_id;
    private String p_name;
    private String p_det;
    private int p_cant;
    private String p_ffab;
    private String p_venc;
    private int p_category;
    private int p_available;
    private String p_time;
    private int f_id;
    private String f_name;
    private String f_det;
    private String f_tel;
    private String f_dir;
    private int f_mun;
    private String f_pic;
    private int c_id;
    private String c_name;
    private String c_det;
    private String c_time;

    public Productos(int pr_id, int pr_farmid, int pr_prodid, int pr_cantdisp, float pr_precio, int pr_preciodesc, int pr_available,
                     String pr_timeCreation, int p_id, String p_name, String p_det, int p_cant, String p_ffab, String p_venc, int p_category,
                     int p_available, String p_time, int f_id, String f_name, String f_det, String f_tel, String f_dir, int f_mun, String f_pic,
                     int c_id, String c_name, String c_det, String c_time)
    {
        this.pr_id = pr_id;
        this.pr_farmid = pr_farmid;
        this.pr_prodid = pr_prodid;
        this.pr_cantdisp = pr_cantdisp;
        this.pr_precio = pr_precio;
        this.pr_preciodesc = pr_preciodesc;
        this.pr_available = pr_available;
        this.pr_timeCreation = pr_timeCreation;
        this.p_id = p_id;
        this.p_name = p_name;
        this.p_det = p_det;
        this.p_cant = p_cant;
        this.p_ffab = p_ffab;
        this.p_venc = p_venc;
        this.p_category = p_category;
        this.p_available = p_available;
        this.p_time = p_time;
        this.f_id = f_id;
        this.f_name = f_name;
        this.f_det = f_det;
        this.f_tel = f_tel;
        this.f_dir = f_dir;
        this.f_mun = f_mun;
        this.f_pic = f_pic;
        this.c_id = c_id;
        this.c_name = c_name;
        this.c_det = c_det;
        this.c_time = c_time;
    }

    public int getPr_id() {
        return pr_id;
    }

    public int getPr_farmid() {
        return pr_farmid;
    }

    public int getPr_prodid() {
        return pr_prodid;
    }

    public int getPr_cantdisp() {
        return pr_cantdisp;
    }

    public float getPr_precio() {
        return pr_precio;
    }

    public int getPr_preciodesc() {
        return pr_preciodesc;
    }

    public int getPr_available() {
        return pr_available;
    }

    public String getPr_timeCreation() {
        return pr_timeCreation;
    }

    public int getP_id() {
        return p_id;
    }

    public String getP_name() {
        return p_name;
    }

    public String getP_det() {
        return p_det;
    }

    public int getP_cant() {
        return p_cant;
    }

    public String getP_ffab() {
        return p_ffab;
    }

    public String getP_venc() {
        return p_venc;
    }

    public int getP_category() {
        return p_category;
    }

    public int getP_available() {
        return p_available;
    }

    public String getP_time() {
        return p_time;
    }

    public int getF_id() {
        return f_id;
    }

    public String getF_name() {
        return f_name;
    }

    public String getF_det() {
        return f_det;
    }

    public String getF_tel() {
        return f_tel;
    }

    public String getF_dir() {
        return f_dir;
    }

    public int getF_mun() {
        return f_mun;
    }

    public String getF_pic() {
        return f_pic;
    }

    public int getC_id() {
        return c_id;
    }

    public String getC_name() {
        return c_name;
    }

    public String getC_det() {
        return c_det;
    }

    public String getC_time() {
        return c_time;
    }
}
