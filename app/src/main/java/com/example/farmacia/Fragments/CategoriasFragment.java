package com.example.farmacia.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmacia.Adaptadores.AdapdatorCategorias;
import com.example.farmacia.Entidades.Categorias;
import com.example.farmacia.Interfaces.Icategorias;
import com.example.farmacia.R;
import com.example.farmacia.conexion.Conexion;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriasFragment extends Fragment implements SearchView.OnQueryTextListener {

    ArrayList<Categorias> listacategorias;
    RecyclerView rvcc;
    AdapdatorCategorias adapdatorCategorias;
    Conexion conexion;
    SearchView searchView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.categorias_fragment,container,false);
        rvcc=view.findViewById(R.id.rcvcategorias);
        listacategorias=new ArrayList<>();
        rvcc.setLayoutManager(new LinearLayoutManager(getContext()));
        conexion=new Conexion();
        searchView=view.findViewById(R.id.searchcategorias);



        Icategorias icategorias=conexion.getRetrofit().create(Icategorias.class);
        Call<List<Categorias>> call=icategorias.listCategorias();
        call.enqueue(new Callback<List<Categorias>>() {
            @Override
            public void onResponse(Call<List<Categorias>> call, Response<List<Categorias>> response) {
                if(response.isSuccessful()){
                    try {

                        for (Categorias categorias:response.body()){
                            listacategorias.add(categorias);
                        }
                        adapdatorCategorias=new AdapdatorCategorias(listacategorias);
                        rvcc.setAdapter(adapdatorCategorias);
                    }catch (Exception e){
                        Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getContext(),"respuesta fallida",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Categorias>> call, Throwable t) {
                Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        searchView.setOnQueryTextListener(this);





        return view;

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapdatorCategorias.filtrado(s);
        return false;
    }
}
