package com.example.farmacia.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.farmacia.Interfaces.Icomunicafragments;
import com.example.farmacia.R;

public class InfoProductoFragment extends Fragment {

    Icomunicafragments icomunicafragments;
    Activity activity;
    Button btn1;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.infoproductofragment,container,false);
        btn1=view.findViewById(R.id.btncambiafragment);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icomunicafragments.enviar();
            }
        });
        return view;
    }

    public InfoProductoFragment(Icomunicafragments icomunicafragments){
        this.icomunicafragments=icomunicafragments;
    }
}
