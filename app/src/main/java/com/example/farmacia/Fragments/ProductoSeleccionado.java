package com.example.farmacia.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.farmacia.Adaptadores.ViewPagerAdpater;
import com.example.farmacia.Interfaces.Icomunicafragments;
import com.example.farmacia.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class ProductoSeleccionado extends Fragment {

    Icomunicafragments icomunicafragments;
    ViewPagerAdpater viewPagerAdpater;
    TabLayout tabLayout;
    ViewPager2 viewPager2;
    private String[] titulos=new String[]{"info","farmacia"};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.productoseleccionado,container,false);
        viewPager2=view.findViewById(R.id.viewpagerp);
        tabLayout=view.findViewById(R.id.tablayout);
        viewPagerAdpater=new ViewPagerAdpater(getActivity(),icomunicafragments);

        viewPager2.setAdapter(viewPagerAdpater);


        new TabLayoutMediator(tabLayout,viewPager2,((tab,position)->tab.setText(titulos[position]))).attach();
        return view;
    }

    public ProductoSeleccionado(Icomunicafragments icomunicafragments){
        this.icomunicafragments=icomunicafragments;
    }


}
