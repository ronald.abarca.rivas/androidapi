package com.example.farmacia.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmacia.Adaptadores.AdapdatorCategorias;
import com.example.farmacia.Adaptadores.AdaptadorProductos;
import com.example.farmacia.Entidades.Categorias;
import com.example.farmacia.Entidades.Productos;
import com.example.farmacia.Interfaces.Iprodcutos;
import com.example.farmacia.R;
import com.example.farmacia.conexion.Conexion;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {

    RecyclerView rcvproductos;
    AdaptadorProductos adaptadorProductos;
    ArrayList<Productos> listaproductos;
    Conexion conexion;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.main_fragment,container,false);

        rcvproductos=view.findViewById(R.id.rcvProductos);
        rcvproductos.setLayoutManager(new LinearLayoutManager(getContext()));
        listaproductos=new ArrayList<>();
        conexion=new Conexion();

        Iprodcutos iprodcutos=conexion.getRetrofit().create(Iprodcutos.class);
        Call<List<Productos>> call=iprodcutos.listProductos();
        call.enqueue(new Callback<List<Productos>>() {
            @Override
            public void onResponse(Call<List<Productos>> call, Response<List<Productos>> response) {
                if(response.isSuccessful()){
                    try {

                        for (Productos productos:response.body()){
                            listaproductos.add(productos);
                        }
                        adaptadorProductos=new AdaptadorProductos(listaproductos);
                        rcvproductos.setAdapter(adaptadorProductos);
                    }catch (Exception e){
                        Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getContext(),"respuesta fallida",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Productos>> call, Throwable t) {
                Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        return view;

    }
}
